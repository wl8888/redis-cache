package com.wxlalt.rediscache.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxlalt.rediscache.entity.Result;
import com.wxlalt.rediscache.entity.Shop;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2021-12-22
 */
public interface IShopService extends IService<Shop> {

    Result queryById(Long id);

    Result update(Shop shop);

    void saveShop2Redis(Long id, Long expireSeconds);
}
