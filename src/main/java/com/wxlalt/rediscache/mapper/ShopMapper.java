package com.wxlalt.rediscache.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wxlalt.rediscache.entity.Shop;



public interface ShopMapper extends BaseMapper<Shop> {

}
