package com.wxlalt.rediscache;

import com.wxlalt.rediscache.service.IShopService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RediscacheApplicationTests {

    @Test
    void contextLoads() {
    }
    @Autowired
    private IShopService shopService;
    @Test
    void test() {
        shopService.saveShop2Redis(1L,10L);
    }
}
