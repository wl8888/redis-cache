# 一 Redis缓存简介
# 二 Redis缓存入门
在我们查询数据一般都是直接查询数据库，返回给前端。为了提高效率实际项目中会将基础数据等热点数据放入redis缓存中。
![在这里插入图片描述](https://img-blog.csdnimg.cn/92591d7ece83407584040cfafc2dbf18.png)
发起请求，先访问redis缓存，缓存中有直接将数据返回；缓存没有命中，查询数据库，将数据存入到Redis中后，再返回数据；

![在这里插入图片描述](https://img-blog.csdnimg.cn/5b7b7230e7f5413f962948c33a818e6f.png)

下面我们模拟查询商铺来实现缓存
![在这里插入图片描述](https://img-blog.csdnimg.cn/0a301de2ccc345f4b02f7f62a1af1626.png)

## 2.1 修改接口queryShopById

```java
@Resource
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public Result queryById(Long id) {
        String key ="cache:shop" + id;
        //1.从redis中查询缓存
        String shopjson = stringRedisTemplate.opsForValue().get(key);
        //2、判断缓存是否存在
        if (StrUtil.isNotBlank(shopjson)) {
            //3、存在，返回数据
            Shop shop = JSONUtil.toBean(shopjson, Shop.class);
            return Result.ok(shop);
        }
        //4.不存在查询数据库
        Shop shop = getById(id);
        //5.不存在返回错误
        if ((null == shop)) {
            return Result.fail("店铺不存在");
        }
        //6.存在放入缓存中
        stringRedisTemplate.opsForValue().set(key,JSONUtil.toJsonStr(shop));
        //7.返回
        return Result.ok(shop);
    }
```
## 2.2 访问观察结果
多次访问 http://localhost:8081/shop/1发现数据能够正常返回
![在这里插入图片描述](https://img-blog.csdnimg.cn/9a89be10df9641eabe6a895936e87122.png)
观察ideal控制台
![在这里插入图片描述](https://img-blog.csdnimg.cn/fb1fe8b86f3f425896d1e822e1d7c89d.png)

观察redis控制台
![在这里插入图片描述](https://img-blog.csdnimg.cn/30b921338b584a2494a864bcc4a2dc03.png)

# 三 缓存更新策略

![在这里插入图片描述](https://img-blog.csdnimg.cn/36dcd1b6f85f41a388b3e09b1a79f57f.png)

- 内存淘汰
  > redis用来解决内存不足的问题，redis是基于内存存储的，内存是有限的，往往会设置上限。我们存储的数据越来越多，可能会导致内存不足，于是redis中会有内存淘汰机制；当内存不足的时候就会触发这种策略，把一部分数据淘汰掉，清空一段空间来存储新的数据。这种机制默认开启。这种数据一致性比较差，只有那些被淘汰的数据才会重新更新，当内存比较充足的时候，缓存数据往往很长一段时间都是旧的数据。

- 超时剔除
  >给缓存数据添加一个过期时间，到期后自动删除数据，下次查询时更新数据；但是在过期时间内不能保证数据的一致性，所以一致性一般，但要好于内存淘汰机制。

- 主动更新

  >自己编写业务逻辑，每当修改数据的同时，缓存也改了。这种数据一致性是比较好的，但也不能完全保证数据完全一致。成本相对较高；

## 主动更新策略
**主动更新策略有三种：**
![在这里插入图片描述](https://img-blog.csdnimg.cn/ff3d051f45c54af39359ba606a66ebef.png)

- `Cache Aside Pattern`
  >由缓存的调用者在更新数据库的同时更新缓存，这种方法需编程实现，业务比较复杂

- `Read/Write Through Pattern`
  >缓存与数据库整合为一个服务,由服务来维护一致性。调用者调用该服务，无需关心缓存一致性问题。这个方法最大的问题就是维护这样一个服务比较复杂，当前市面上没有完善的解决方案

- `Write Behind Caching Pattern`
  > 调用者只操作缓存，由其它线程异步的将缓存数据持久化到数据库，保证最终一致。如果异步线程还没有将最新操作持久化到数据库中redis就宕机了，会导致数据丢失

其中**Cache Aside Pattern是最受欢迎的更新策略**，但操作缓存和数据库时有三个问题需要考虑:
![在这里插入图片描述](https://img-blog.csdnimg.cn/629a9177c2ec419fb4045b435f646b3d.png)
### 先删除缓存，再操作数据库和先操作数据库，再删除缓存两种方案进行对比

**先删除缓存，再操作数据库**可能会导致下图问题，就是当线程1删除缓存后，还没有更新完数据库，线程2就来查询，发现redis没有，就去查询数据库，将脏数据写入缓存，这将导致后续所有访问都是获取redis脏数据。
==这种线程安全发生数据库数据和缓存数据不一致的情况发生的概率还是挺高的，因为删缓存很快，然后期间会对数据进行处理最后再更新数据，这时间差越长其他线程乘虚而入的机会就大；==
![在这里插入图片描述](https://img-blog.csdnimg.cn/6dacb6c7c8744345bbf8fb578b1a1aab.png)

**先操作数据库，再删除缓存**假如某数据没有缓存，线程1就去数据库读取数据，在它尚未将其写入redis的时候，线程2更新了数据库，然后在线程1还没有将旧数据写入redis的时候执行了删除缓存操作，这当然是无效删除，然后线程1再将旧数据写入redis，这也会导致后续的所有请求都拿到脏数据；
**这种情况发生数据不一致得情况高不高呢？发生的条件要有这两个：**
>一 首先需要两个线程的并行运行
二 线程1来查询的时候恰好缓存失效了，**然后去查询数据库，去写缓存的期间（时间差极短）**，突然来了一个线程进行更新数据库操作

==在这极短的时间内要完成数据库写的操作，可能性很低，所以出现数据不一致的情况不高；==
![在这里插入图片描述](https://img-blog.csdnimg.cn/f4a1c4e88d7848d19e7f9a559738b8d8.png)

### 延迟双删
上述两种情况都有可能导致数据不一致，那么有没有更好的方法保证一致性呢？

延时双删常用步骤有 4 个，参考下面伪代码：

```cpp
def update_data(key, obj):
    del_cache(key)     # 删除 redis 缓存数据。
    update_db(obj)     # 更新数据库数据。
    logic_sleep(_time) # 当前逻辑延时执行。
    del_cache(key)     # 删除 redis 缓存数据。
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/cecdbcc293784afe8ba57359bc8560ac.png)

logic_sleep是当前请求逻辑延时执行，例如：协程睡眠切换，或者异步逻辑放进时钟里延时执行下一个步骤。很多人会误认为这是线程/进程睡眠切换，当然这样也行，不觉得这样影响实在太大了么~
[redis缓存一致性延时双删代码实现方式](https://www.jb51.net/article/260801.htm)
![在这里插入图片描述](https://img-blog.csdnimg.cn/cf1dea46fcd1454f81a3443fcfe87990.png)
==为何要延时？==
>这是为了我们在第二次删除redis之前能完成数据库的更新操作。假象一下，如果没有第三步操作时，有很大概率，在两次删除redis操作执行完毕之后，数据库的数据还没有更新，此时若有请求访问数据，便会出现我们一开始提到的那个问题。

==为何要两次删除缓存?==
>如果我们没有第二次删除操作，此时有请求访问数据，有可能是访问的之前未做修改的redis数据，删除操作执行后，redis为空，有请求进来时，便会去访问数据库，此时数据库中的数据已是更新后的数据，保证了数据的一致性。

**小节**

- 延迟双删用比较简洁的方式实现 mysql 和 redis **数据最终一致性**，但它不是强一致。
- 延迟，是因为 mysql 和 redis 主从节点数据同步不是实时的，所以需要等待一段时间，去增强它们的数据一致性。
- 延迟是指当前请求逻辑处理延时，而不是当前线程或进程睡眠延迟。
- mysql 和 redis 数据一致性是一个复杂的课题，通常是多种策略同时使用，例如：延迟双删、redis
  过期淘汰、通过路由策略串行处理同类型数据、分布式锁等等。

参考连接
[redis的延迟双删策略总结](https://blog.csdn.net/miaoyitao/article/details/123606389)
[延时双删（redis-mysql）数据一致性思考](https://zhuanlan.zhihu.com/p/467410359)
## 缓存更新策略总结
![在这里插入图片描述](https://img-blog.csdnimg.cn/68b2cfc8b9f747ddaabc3cb8533f5f47.png)

## 案例实践
**实现商铺和缓存与数据库双写一致**
>根据id查询店铺时，如果缓存未命中，则查询数据库，将数据库结果写入缓存，并设置超时时间
根据id修改店铺时，先修改数据库，再删除缓存


**修改重点代码1：** 修改ShopServiceImpl的queryById方法
**设置redis缓存时添加过期时间**

```java
    public static final Long CACHE_SHOP_TTL = 30L;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Result queryById(Long id) {
        String key = CACHE_SHOP_KEY + id;
        // 1.从redis查询商铺缓存
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (StrUtil.isNotBlank(shopJson)) {
            // 3.存在，直接返回
            Shop shop = JSONUtil.toBean(shopJson, Shop.class);
            return Result.ok(shop);
        }
  
        // 4.不存在，根据id查询数据库
        Shop shop = getById(id);
        // 5.不存在，返回错误
        if (shop == null) {
           // 返回错误信息
            return Result.fail("店铺不存在！");
        }
        // 6.存在，写入redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), CACHE_SHOP_TTL, TimeUnit.MINUTES);
        // 7.返回
        return Result.ok(shop);
    }
```
**修改重点代码2**

代码分析：通过之前的淘汰，我们确定了采用删除策略，来解决双写问题，当我们修改了数据之后，然后把缓存中的数据进行删除，查询时发现缓存中没有数据，则会从mysql中加载最新的数据，从而避免数据库和缓存不一致的问题

```java
 	@Override
    @Transactional
    public Result update(Shop shop) {
        Long id = shop.getId();
        if (id == null) {
            return Result.fail("店铺id不能为空");
        }
        // 1.更新数据库
        updateById(shop);
        // 2.删除缓存
        stringRedisTemplate.delete(CACHE_SHOP_KEY + id);
        return Result.ok();
    }
```

# 四 缓存穿透

## 4.1 缓存穿透分析及解决方案

==**缓存穿透** ：缓存穿透是指客户端请求的数据在缓存中和数据库中都不存在，这样缓存永远不会生效，这些请求都会打到数据库。==

常见的解决方案有两种：

* 缓存空对象
    * 优点：实现简单，维护方便
    * 缺点：
        * 额外的内存消耗
        * 可能造成短期的不一致
* 布隆过滤
    * 优点：内存占用较少，没有多余key
    * 缺点：
        * 实现复杂
        * 存在误判可能



**缓存空对象思路分析：** 当我们客户端访问不存在的数据时，先请求redis，但是此时redis中没有数据，此时会访问到数据库，但是数据库中也没有数据，这个数据穿透了缓存，直击数据库，我们都知道数据库能够承载的并发不如redis这么高，如果大量的请求同时过来访问这种不存在的数据，这些请求就都会访问到数据库，简单的解决方案就是哪怕这个数据在数据库中也不存在，我们也把这个数据存入到redis中去，这样，下次用户过来访问这个不存在的数据，那么在redis中也能找到这个数据就不会进入到缓存了



**布隆过滤：** 布隆过滤器其实采用的是哈希思想来解决这个问题，通过一个庞大的二进制数组，走哈希思想去判断当前这个要查询的这个数据是否存在，如果布隆过滤器判断存在，则放行，这个请求会去访问redis，哪怕此时redis中的数据过期了，但是数据库中一定存在这个数据，在数据库中查询出来这个数据后，再将其放入到redis中，

假设布隆过滤器判断这个数据不存在，则直接返回

这种方式优点在于节约内存空间，存在误判，误判原因在于：布隆过滤器走的是哈希思想，只要哈希思想，就可能存在哈希冲突
![在这里插入图片描述](https://img-blog.csdnimg.cn/d73203260903459db6489f8f9f1d12c8.png)
## 4.2 缓存穿透解决实践
**核心思路如下：**
- **原来的逻辑中**：我们如果发现这个数据在mysql中不存在，直接就返回404了，这样是会存在缓存穿透问题的
- **现在的逻辑中**：如果这个数据不存在，我们不会返回404 ，还是会把这个数据写入到Redis中，并且将value设置为空，欧当再次发起查询时，我们如果发现命中之后，判断这个value是否是null，如果是null，则是之前写入的数据，证明是缓存穿透数据，如果不是，则直接返回数据。
  ![在这里插入图片描述](https://img-blog.csdnimg.cn/0787febcd52840289abc07c4bc640a46.png)

修改ShopServiceImpl的queryById方法
将空值写入redis，空值的缓存时间应该小于正常值的时间
```java
@Override
    public Result queryById(Long id) {
        String key = CACHE_SHOP_KEY + id;
        // 1.从redis查询商铺缓存
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (StrUtil.isNotBlank(shopJson)) {
            // 3.存在，直接返回
            Shop shop = JSONUtil.toBean(shopJson, Shop.class);
            return Result.ok(shop);
        }
        // 判断命中的是否是空值
        if (shopJson != null) {
            // 返回一个错误信息
            return Result.fail("店铺信息不存在！");
        }

        // 4.不存在，根据id查询数据库
        Shop shop = getById(id);
        // 5.不存在，返回错误
        if (shop == null) {
            // 将空值写入redis，空值的缓存时间应该小于正常值的时间
            stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
            // 返回错误信息
            return Result.fail("店铺不存在！");
        }
        // 6.存在，写入redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), CACHE_SHOP_TTL, TimeUnit.MINUTES);
        // 7.返回
        return Result.ok(shop);
    }
```

## 4.3 总结
缓存穿透产生的原因是什么？

* 用户请求的数据在缓存中和数据库中都不存在，不断发起这样的请求，给数据库带来巨大压力

缓存穿透的解决方案有哪些？

* 缓存null值
* 布隆过滤
* 增强id的复杂度，避免被猜测id规律
* 做好数据的基础格式校验
* 加强用户权限校验
* 做好热点参数的限流
# 五 缓存雪崩

==**缓存雪崩**是指在同一时段大量的缓存key同时失效或者Redis服务宕机，导致大量请求到达数据库，带来巨大压力。==

解决方案：

* 给不同的Key的TTL添加随机值
* 利用Redis集群提高服务的可用性
* 给缓存业务添加降级限流策略
* 给业务添加多级缓存

![在这里插入图片描述](https://img-blog.csdnimg.cn/3a5e88a489f44c8d889bac6fb66fd21f.png)

# 六 缓存击穿
==**缓存击穿问题**也叫热点Key问题，就是一个被高并发访问并且缓存重建业务较复杂的key突然失效了，无数的请求访问会在瞬间给数据库带来巨大的冲击。==

常见的解决方案有两种：

* 互斥锁
* 逻辑过期

## 6.1 逻辑分析
>假设线程1在查询缓存之后，本来应该去查询数据库，然后把这个数据重新加载到缓存的，此时只要线程1走完这个逻辑，其他线程就都能从缓存中加载这些数据了，但是假设在线程1没有走完的时候，后续的线程2，线程3，线程4同时过来访问当前这个方法， 那么这些线程都不能从缓存中查询到数据，那么他们就会同一时刻来访问查询缓存，都没查到，接着同一时间去访问数据库，同时的去执行数据库代码，对数据库访问压力过大

![在这里插入图片描述](https://img-blog.csdnimg.cn/b1f058e163784ba4b115158eaccd3e1f.png)





## 6.2 解决方案一、使用锁来解决：

因为锁能实现互斥性。假设线程过来，只能一个人一个人的来访问数据库，从而避免对于数据库访问压力过大，但这也会影响查询的性能，因为此时会让查询的性能从并行变成了串行，我们可以采用tryLock方法 + double check来解决这样的问题。

假设现在线程1过来访问，他查询缓存没有命中，但是此时他获得到了锁的资源，那么线程1就会一个人去执行逻辑，假设现在线程2过来，线程2在执行过程中，并没有获得到锁，那么线程2就可以进行到休眠，直到线程1把锁释放后，线程2获得到锁，然后再来执行逻辑，此时就能够从缓存中拿到数据了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/be42a9fd010c45188743e760b6545167.png)


## 6.3 解决方案二、逻辑过期方案

**方案分析**：
>我们之所以会出现这个缓存击穿问题，主要原因是在于我们对key设置了过期时间，假设我们不设置过期时间，其实就不会有缓存击穿的问题，但是不设置过期时间，这样数据不就一直占用我们内存了吗，我们可以采用逻辑过期方案。

我们把过期时间设置在 redis的value中，注意：这个过期时间并不会直接作用于redis，而是我们后续通过逻辑去处理。假设线程1去查询缓存，然后从value中判断出来当前的数据已经过期了，此时线程1去获得互斥锁，那么其他线程会进行阻塞，**获得了锁的线程他会开启一个线程去进行以前的重构数据的逻辑**，直到新开的线程完成这个逻辑后，才释放锁，而线程1直接进行返回，假设现在线程3过来访问，由于线程线程2持有着锁，所以线程3无法获得锁，线程3也直接返回数据，只有等到新开的线程2把重建数据构建完后，其他线程才能走返回正确的数据。

**这种方案巧妙在于，异步的构建缓存，缺点在于在构建完缓存之前，返回的都是脏数据**。

![在这里插入图片描述](https://img-blog.csdnimg.cn/3c44f5cbf53e410ebaa632e7dcb06917.png)


进行对比

**互斥锁方案：**由于保证了互斥性，所以数据一致，且实现简单，因为仅仅只需要加一把锁而已，也没其他的事情需要操心，所以没有额外的内存消耗，缺点在于有锁就有死锁问题的发生，且只能串行执行性能肯定受到影响

**逻辑过期方案：** 线程读取过程中不需要等待，性能好，有一个额外的线程持有锁去进行重构数据，但是在重构数据完成前，其他的线程只能返回之前的数据，且实现起来麻烦

![在这里插入图片描述](https://img-blog.csdnimg.cn/dc91e261c5c94eb1b449b2eac14f03bf.png)
## 6.4 利用互斥锁解决缓存击穿问题实践
**核心思路**：
>相较于原来从缓存中查询不到数据后直接查询数据库而言，现在的方案是进行查询之后，如果从缓存没有查询到数据，则进行互斥锁的获取，获取互斥锁后，判断是否获得到了锁，如果没有获得到，则休眠，过一会再进行尝试，直到获取到锁为止，才能进行查询
如果获取到了锁的线程，再去进行查询，查询后将数据写入redis，再释放锁，返回数据，利用互斥锁就能保证只有一个线程去执行操作数据库的逻辑，防止缓存击穿

![在这里插入图片描述](https://img-blog.csdnimg.cn/f6ce1c236b044aa98236619b2a12d38a.png)


**操作锁的代码：**

核心思路就是**利用redis的setnx方法来表示获取锁**，==该方法含义是redis中如果没有这个key，则插入成功，返回1，在stringRedisTemplate中返回true，  如果有这个key则插入失败，则返回0， 在stringRedisTemplate返回false==，我们可以通过true，或者是false，来表示是否有线程成功插入key，成功插入的key的线程我们认为他就是获得到锁的线程。

```java

private boolean tryLock(String key) {
    Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10, TimeUnit.SECONDS);
    return BooleanUtil.isTrue(flag);
}

private void unlock(String key) {
    stringRedisTemplate.delete(key);
}
```

**操作代码：**

```java
 public Shop queryWithMutex(Long id)  {
        String key = CACHE_SHOP_KEY + id;
        // 1、从redis中查询商铺缓存
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        // 2、判断是否存在
        if (StrUtil.isNotBlank(shopJson)) {
            // 存在,直接返回
            return JSONUtil.toBean(shopJson, Shop.class);
        }
        //判断命中的值是否是空值
        if (shopJson != null) {
            //返回一个错误信息
            return null;
        }
        // 4.实现缓存重构
        //4.1 获取互斥锁
        String lockKey = "lock:shop:" + id;
        Shop shop = null;
        try {
            boolean isLock = tryLock(lockKey);
            // 4.2 判断否获取成功
            if(!isLock){
                //4.3 失败，则休眠重试
                Thread.sleep(50);
                return queryWithMutex(id);
            }
            //4.4 成功，根据id查询数据库
             shop = getById(id);
            // 5.不存在，返回错误
            if(shop == null){
                 //将空值写入redis
                stringRedisTemplate.opsForValue().set(key,"",CACHE_NULL_TTL,TimeUnit.MINUTES);
                //返回错误信息
                return null;
            }
            //6.写入redis
            stringRedisTemplate.opsForValue().set(key,JSONUtil.toJsonStr(shop),CACHE_NULL_TTL,TimeUnit.MINUTES);

        }catch (Exception e){
            throw new RuntimeException(e);
        }
        finally {
            //7.释放互斥锁
            unlock(lockKey);
        }
        return shop;
    }
```

### jmeter测试

多次访问模拟并发
http://localhost:8081/shop/1
观察控制台，查询sql只执行了一次
![在这里插入图片描述](https://img-blog.csdnimg.cn/e1e41cb1ae6548dba955480cb1ef7b44.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2f298fc1c1654678a3261a0541f83c2a.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/9572a9e6bc484d96b4c5d7538423d313.png)

## 6.5 利用逻辑过期解决缓存击穿问题实践

**需求：修改根据id查询商铺的业务，基于逻辑过期方式来解决缓存击穿问题**

思路分析：当用户开始查询redis时，判断是否命中，如果没有命中则直接返回空数据，不查询数据库，而一旦命中后，将value取出，判断value中的过期时间是否满足，如果没有过期，则直接返回redis中的数据，**如果过期，则在开启独立线程后直接返回之前的数据，独立线程去重构数据，重构完成后释放互斥锁。**
![在这里插入图片描述](https://img-blog.csdnimg.cn/8615a466186e4843992cb7fadb0873ae.png)

如果封装数据：因为现在redis中存储的数据的value需要带上过期时间，此时要么去修改原来的实体类，要么新建一个实体类。

**步骤一、**

我们采用第二个方案，这个方案，对原来代码没有侵入性。

```java
@Data
public class RedisData {
    private LocalDateTime expireTime;
    private Object data;
}
```

**步骤二、**

在**ShopServiceImpl** 新增此方法，利用单元测试进行缓存预热

```java
    @Override
    public void saveShop2Redis(Long id, Long expireSeconds) {
        Shop shop = getById(id);
        RedisData redisData = new RedisData();
        redisData.setData(shop);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(expireSeconds));
        stringRedisTemplate.opsForValue().set(CACHE_SHOP_KEY + id, JSONUtil.toJsonStr(redisData));
    }
```


**在测试类中**

```java
    @Autowired
    private IShopService shopService;
    @Test
    void test() {
        shopService.saveShop2Redis(1L,10L);
    }

```
![在这里插入图片描述](https://img-blog.csdnimg.cn/c1e6b6f7853d4f6ca8406d86fe4368e4.png)

步骤三：正式代码

**ShopServiceImpl**

```java
private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);
public Shop queryWithLogicalExpire( Long id ) {
    String key = CACHE_SHOP_KEY + id;
    // 1.从redis查询商铺缓存
    String json = stringRedisTemplate.opsForValue().get(key);
    // 2.判断是否存在
    if (StrUtil.isBlank(json)) {
        // 3.存在，直接返回
        return null;
    }
    // 4.命中，需要先把json反序列化为对象
    RedisData redisData = JSONUtil.toBean(json, RedisData.class);
    Shop shop = JSONUtil.toBean((JSONObject) redisData.getData(), Shop.class);
    LocalDateTime expireTime = redisData.getExpireTime();
    // 5.判断是否过期
    if(expireTime.isAfter(LocalDateTime.now())) {
        // 5.1.未过期，直接返回店铺信息
        return shop;
    }
    // 5.2.已过期，需要缓存重建
    // 6.缓存重建
    // 6.1.获取互斥锁
    String lockKey = LOCK_SHOP_KEY + id;
    boolean isLock = tryLock(lockKey);
    // 6.2.判断是否获取锁成功
    if (isLock){
        CACHE_REBUILD_EXECUTOR.submit( ()->{

            try{
                //重建缓存
                this.saveShop2Redis(id,20L);
            }catch (Exception e){
                throw new RuntimeException(e);
            }finally {
                unlock(lockKey);
            }
        });
    }
    // 6.4.返回过期的商铺信息
    return shop;
}
```
# 七 缓存工具封装
基于StringRedisTemplate封装一个缓存工具类，满足下列需求：

* 方法1：将任意Java对象序列化为json并存储在string类型的key中，并且可以设置TTL过期时间
* 方法2：将任意Java对象序列化为json并存储在string类型的key中，并且可以设置逻辑过期时间，用于处理缓

存击穿问题

* 方法3：根据指定的key查询缓存，并反序列化为指定类型，利用缓存空值的方式解决缓存穿透问题
* 方法4：根据指定的key查询缓存，并反序列化为指定类型，需要利用逻辑过期解决缓存击穿问题

将逻辑进行封装

```java
@Slf4j
@Component
public class CacheClient {

    private final StringRedisTemplate stringRedisTemplate;

    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

    public CacheClient(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public void set(String key, Object value, Long time, TimeUnit unit) {
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value), time, unit);
    }

    public void setWithLogicalExpire(String key, Object value, Long time, TimeUnit unit) {
        // 设置逻辑过期
        RedisData redisData = new RedisData();
        redisData.setData(value);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(unit.toSeconds(time)));
        // 写入Redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(redisData));
    }

    public <R,ID> R queryWithPassThrough(
            String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit unit){
        String key = keyPrefix + id;
        // 1.从redis查询商铺缓存
        String json = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (StrUtil.isNotBlank(json)) {
            // 3.存在，直接返回
            return JSONUtil.toBean(json, type);
        }
        // 判断命中的是否是空值
        if (json != null) {
            // 返回一个错误信息
            return null;
        }

        // 4.不存在，根据id查询数据库
        R r = dbFallback.apply(id);
        // 5.不存在，返回错误
        if (r == null) {
            // 将空值写入redis
            stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
            // 返回错误信息
            return null;
        }
        // 6.存在，写入redis
        this.set(key, r, time, unit);
        return r;
    }

    public <R, ID> R queryWithLogicalExpire(
            String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit unit) {
        String key = keyPrefix + id;
        // 1.从redis查询商铺缓存
        String json = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (StrUtil.isBlank(json)) {
            // 3.存在，直接返回
            return null;
        }
        // 4.命中，需要先把json反序列化为对象
        RedisData redisData = JSONUtil.toBean(json, RedisData.class);
        R r = JSONUtil.toBean((JSONObject) redisData.getData(), type);
        LocalDateTime expireTime = redisData.getExpireTime();
        // 5.判断是否过期
        if(expireTime.isAfter(LocalDateTime.now())) {
            // 5.1.未过期，直接返回店铺信息
            return r;
        }
        // 5.2.已过期，需要缓存重建
        // 6.缓存重建
        // 6.1.获取互斥锁
        String lockKey = LOCK_SHOP_KEY + id;
        boolean isLock = tryLock(lockKey);
        // 6.2.判断是否获取锁成功
        if (isLock){
            // 6.3.成功，开启独立线程，实现缓存重建
            CACHE_REBUILD_EXECUTOR.submit(() -> {
                try {
                    // 查询数据库
                    R newR = dbFallback.apply(id);
                    // 重建缓存
                    this.setWithLogicalExpire(key, newR, time, unit);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }finally {
                    // 释放锁
                    unlock(lockKey);
                }
            });
        }
        // 6.4.返回过期的商铺信息
        return r;
    }

    public <R, ID> R queryWithMutex(
            String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit unit) {
        String key = keyPrefix + id;
        // 1.从redis查询商铺缓存
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (StrUtil.isNotBlank(shopJson)) {
            // 3.存在，直接返回
            return JSONUtil.toBean(shopJson, type);
        }
        // 判断命中的是否是空值
        if (shopJson != null) {
            // 返回一个错误信息
            return null;
        }

        // 4.实现缓存重建
        // 4.1.获取互斥锁
        String lockKey = LOCK_SHOP_KEY + id;
        R r = null;
        try {
            boolean isLock = tryLock(lockKey);
            // 4.2.判断是否获取成功
            if (!isLock) {
                // 4.3.获取锁失败，休眠并重试
                Thread.sleep(50);
                return queryWithMutex(keyPrefix, id, type, dbFallback, time, unit);
            }
            // 4.4.获取锁成功，根据id查询数据库
            r = dbFallback.apply(id);
            // 5.不存在，返回错误
            if (r == null) {
                // 将空值写入redis
                stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
                // 返回错误信息
                return null;
            }
            // 6.存在，写入redis
            this.set(key, r, time, unit);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }finally {
            // 7.释放锁
            unlock(lockKey);
        }
        // 8.返回
        return r;
    }

    private boolean tryLock(String key) {
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    private void unlock(String key) {
        stringRedisTemplate.delete(key);
    }
}
```

在ShopServiceImpl 中

```java
@Resource
private CacheClient cacheClient;

 @Override
    public Result queryById(Long id) {
        // 解决缓存穿透
        Shop shop = cacheClient
                .queryWithPassThrough(CACHE_SHOP_KEY, id, Shop.class, this::getById, CACHE_SHOP_TTL, TimeUnit.MINUTES);

        // 互斥锁解决缓存击穿
        // Shop shop = cacheClient
        //         .queryWithMutex(CACHE_SHOP_KEY, id, Shop.class, this::getById, CACHE_SHOP_TTL, TimeUnit.MINUTES);

        // 逻辑过期解决缓存击穿
        // Shop shop = cacheClient
        //         .queryWithLogicalExpire(CACHE_SHOP_KEY, id, Shop.class, this::getById, 20L, TimeUnit.SECONDS);

        if (shop == null) {
            return Result.fail("店铺不存在！");
        }
        // 7.返回
        return Result.ok(shop);
    }
```
# 八 案例来源
[黑马程序员Redis入门到实战教程，深度透析redis底层原理+redis分布式锁+企业解决方案+黑马点评实战项目](https://www.bilibili.com/video/BV1cr4y1671t?p=39&vd_source=084696e27da8351afcb4e65f1e101150)


